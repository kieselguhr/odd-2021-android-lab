package com.splashscreen.myapplication;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentManager;

import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;

import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.android.material.navigation.NavigationBarView;
import com.splashscreen.myapplication.fragments.BlankFragment;
import com.splashscreen.myapplication.fragments.BlankFragment2;

public class RealNavigationActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_real_navigation);

        FragmentManager fragmentManager = getSupportFragmentManager();
        fragmentManager.beginTransaction()
                .replace(R.id.fragment_container, BlankFragment.class, null)
                .setReorderingAllowed(true)
                .commit();

        BottomNavigationView bnv = findViewById(R.id.bottom_navigation);
        bnv.setOnItemSelectedListener(new NavigationBarView.OnItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                int id = item.getItemId();
                FragmentManager fragmentManager = getSupportFragmentManager();

                switch (id){
                    case R.id.page_1:
                        fragmentManager.beginTransaction()
                                .replace(R.id.fragment_container, BlankFragment.class, null)
                                .setReorderingAllowed(true)
                                .commit();
                        break;
                    case R.id.page_2:
                        fragmentManager.beginTransaction()
                                .replace(R.id.fragment_container, BlankFragment2.class, null)
                                .setReorderingAllowed(true)
                                .commit();
                        break;
                }

//                Toast.makeText(NavigationTemplateActivity.this, "id " + id, Toast.LENGTH_SHORT).show();
                return true;
            }
        });

    }
}