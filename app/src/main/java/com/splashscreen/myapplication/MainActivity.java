package com.splashscreen.myapplication;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        EditText emailEdit = findViewById(R.id.activity_main_email_edit_text);
        EditText passwordEdit = findViewById(R.id.activity_main_password_edit_text);

        Button clickMeButton = findViewById(R.id.activity_main_click_me_button);

        clickMeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String emailText = emailEdit.getText().toString();
                String passwordText = passwordEdit.getText().toString();

                if(emailText.equals("admin") && passwordText.equals("password")){
                    Log.d("MAIN_ACTIVITY", "PASSWORD IS CORRECT");
                    moveToLoginActivity(emailText);
                    Toast.makeText(MainActivity.this, "Password is correct", Toast.LENGTH_SHORT).show();
                }else{
                    Log.d("MAIN_ACTIVITY", "PASSWORD IS FALSE");
                    Toast.makeText(MainActivity.this, "Password is false", Toast.LENGTH_LONG).show();
                }
            }
        });

    }

    private void moveToLoginActivity(String emailText){
        Intent intent = new Intent(this, LoginResultActivity.class);

        Bundle extras = new Bundle();
        extras.putString("username", emailText);

        intent.putExtras(extras);

        startActivity(intent);
    }

}