package com.splashscreen.myapplication;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

public class DetailPageActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_page);

        Bundle params = getIntent().getExtras();

        String title1 = params.getString("tilte");
        String subTitle = params.getString("subtitle");
        int drawableId = params.getInt("DrawableId");

        TextView titleText = findViewById(R.id.detail_act__title_view);
        TextView subTitleText = findViewById(R.id.detail_act__sub_title_view);
        ImageView imageView = findViewById(R.id.detail_act__image_view);

        titleText.setText(title1);
        subTitleText.setText(subTitle);
        imageView.setImageResource(drawableId);

    }
}