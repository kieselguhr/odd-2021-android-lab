package com.splashscreen.myapplication;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.GridLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.Network;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.bumptech.glide.Glide;
import com.splashscreen.myapplication.models.UserModel;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class NetworkActivity extends AppCompatActivity {

    ArrayList<UserModel> userModels = new ArrayList<>();
    RecyclerView rv;
    UserModelAdapter dataModelAdapter;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_network);

        RequestQueue queue = Volley.newRequestQueue(this);
        String url = "https://reqres.in/api/users";

        generateRecyclerView();

        Button deleteButton = findViewById(R.id.delete_btn);
        deleteButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                userModels.clear();
                dataModelAdapter.notifyDataSetChanged();
            }
        });

        StringRequest stringRequest = new StringRequest(Request.Method.GET, url,

                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.d("RESULT", response);

                        try {

                            JSONObject jsonObject = new JSONObject(response);

                            userModels.clear();

                            JSONArray userDataJSONArray = jsonObject.getJSONArray("data");
                            for(int i = 0; i < userDataJSONArray.length(); i++){
                                JSONObject userDataJSONObject = userDataJSONArray.getJSONObject((i));
                                UserModel userModel = new UserModel(
                                        userDataJSONObject.getString("id"),
                                        userDataJSONObject.getString("email"),
                                        userDataJSONObject.getString("first_name"),
                                        userDataJSONObject.getString("last_name"),
                                        userDataJSONObject.getString("avatar")
                                );
                                userModels.add(userModel);
                            }

//                            dataModelAdapter.notifyDataSetChanged();

//                            rv.getAdapter().notifyDataSetChanged();

//                            Glide
//                                    .with(NetworkActivity.this)
//                                    .load("https://reqres.in/img/faces/4-image.jpjkhgkjlhjkhnjkg")
//                                    .circleCrop()
//                                    .placeholder(R.drawable.ic_baseline_lock_24)
//                                    .into(ppImageView);



                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.e("FETCH_ERROR", error.networkResponse.toString());
                    }
                }
        );

        queue.add(stringRequest);
    }

    private void generateRecyclerView(){
        rv = findViewById(R.id.network_act_recycler_view);

//        GridLayoutManager layoutManager = new GridLayoutManager(this, 2);
        LinearLayoutManager layoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        dataModelAdapter = new UserModelAdapter(userModels);

        rv.setAdapter(dataModelAdapter);
        rv.setLayoutManager(layoutManager);
    }

}

class UserModelAdapter extends RecyclerView.Adapter<UserModelViewHolder>{

    ArrayList<UserModel> userModels;

    public UserModelAdapter(ArrayList<UserModel> userModels) {
        this.userModels = userModels;
    }

    public void setUserModels(ArrayList<UserModel> userModels) {
        this.userModels = userModels;
    }

    @NonNull
    @Override
    public UserModelViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        Context context = parent.getContext();
        LayoutInflater layoutInflater = LayoutInflater.from(context);
        View view = layoutInflater.inflate(R.layout.list_view_user_info_view_holder_constraint, parent, false);

        return new UserModelViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull UserModelViewHolder holder, int position) {
        UserModel um = userModels.get(position);
        holder.emailTextView.setText(um.getEmail());
        holder.nameTextView.setText(um.getFirstName() + " " + um.getLastName());
        holder.idTextView.setText("#" + um.getId());

        Glide
                .with(holder.imageView)
                .load(um.getAvatar())
                .circleCrop()
                .placeholder(R.drawable.ic_baseline_lock_24)
                .into(holder.imageView);

    }

    @Override
    public int getItemCount() {
        return userModels.size();
    }
}

class UserModelViewHolder extends RecyclerView.ViewHolder {

    TextView emailTextView;
    TextView nameTextView;
    TextView idTextView;
    ImageView imageView;

//    View rootView;

    public UserModelViewHolder(@NonNull View itemView) {
        super(itemView);
        emailTextView = itemView.findViewById(R.id.user_info_view_email_text);
        nameTextView = itemView.findViewById(R.id.user_info_view_name_text);
        idTextView = itemView.findViewById(R.id.user_info_view_id_text);
        imageView = itemView.findViewById(R.id.user_info_view_profile_image);
    }
}
