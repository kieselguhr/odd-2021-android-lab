package com.splashscreen.myapplication.fragments;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.splashscreen.myapplication.R;
import com.splashscreen.myapplication.models.UserModel;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link BlankFragment2#newInstance} factory method to
 * create an instance of this fragment.
 */
public class BlankFragment2 extends Fragment {

    public BlankFragment2() {
        // Required empty public constructor
    }

    // TODO: Rename and change types and number of parameters
    public static BlankFragment2 newInstance() {
        BlankFragment2 fragment = new BlankFragment2();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment


        return inflater.inflate(R.layout.fragment_blank2, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        TextView tv = view.findViewById(R.id.blank_fragment_text_view);
        tv.setText("Halo Halo Halo");

        ArrayList<UserModel> userModels = new ArrayList<>();


        RequestQueue queue = Volley.newRequestQueue(getContext());
        String url = "https://reqres.in/api/users";

        StringRequest stringRequest = new StringRequest(Request.Method.GET, url,

                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.d("RESULT", response);

                        try {

                            JSONObject jsonObject = new JSONObject(response);

                            userModels.clear();

                            JSONArray userDataJSONArray = jsonObject.getJSONArray("data");
                            for(int i = 0; i < userDataJSONArray.length(); i++){
                                JSONObject userDataJSONObject = userDataJSONArray.getJSONObject((i));
                                UserModel userModel = new UserModel(
                                        userDataJSONObject.getString("id"),
                                        userDataJSONObject.getString("email"),
                                        userDataJSONObject.getString("first_name"),
                                        userDataJSONObject.getString("last_name"),
                                        userDataJSONObject.getString("avatar")
                                );
                                userModels.add(userModel);
                            }

//                            dataModelAdapter.notifyDataSetChanged();

//                            rv.getAdapter().notifyDataSetChanged();

//                            Glide
//                                    .with(NetworkActivity.this)
//                                    .load("https://reqres.in/img/faces/4-image.jpjkhgkjlhjkhnjkg")
//                                    .circleCrop()
//                                    .placeholder(R.drawable.ic_baseline_lock_24)
//                                    .into(ppImageView);



                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.e("FETCH_ERROR", error.networkResponse.toString());
                    }
                }
        );

        queue.add(stringRequest);
    }
}